from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    firstname = models.CharField('Имя', max_length=20)
    lastname = models.CharField('Фамилия', max_length=20)
    middlename = models.CharField('Отчество', max_length=20)


    class Meta:
        verbose_name_plural = 'ФИО'
        verbose_name = 'ФИО'