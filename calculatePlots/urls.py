from django.contrib import admin
from django.urls import path
from calculatePlots.views import Testfunction,All_plots

urlpatterns = [
    path('',Testfunction, name = 'calculating'),
    path('all_plots/',All_plots, name = 'all_plots'),
]