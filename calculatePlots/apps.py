from django.apps import AppConfig


class CalculateplotsConfig(AppConfig):
    name = 'calculatePlots'
