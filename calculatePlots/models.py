from django.db import models
from users.models import CustomUser

class MyModelWithFigure(models.Model):
   name = models.CharField(max_length=50)
   blob = models.BinaryField(blank=True,null=True,default=None)
   author = models.ForeignKey(CustomUser,on_delete=models.CASCADE,db_index=True)