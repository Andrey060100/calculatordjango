from django.shortcuts import render, HttpResponse
from .models import MyModelWithFigure

from .forms import EquationForm
from .MathparseCopy1LOG import mainMathParse as calculate

import io
import urllib,base64


import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot



def Testfunction(request):
        form = EquationForm(request.POST)
        values = request.POST.get('equation')

        xrange1 = request.POST.get('xrange1')
        xrange2 = request.POST.get('xrange2')
        if xrange1!=None and xrange1!='' and xrange1!=None and xrange1!='':
                xrange1 = float(xrange1)
                xrange2 = float(xrange2)
        else:
                xrange1 = 0
                xrange2 = 5
        if values!=None and values!='':
                mathematicEquation = values
                result,error = calculate(mathematicEquation,xrange1,xrange2)

        else:
                mathematicEquation = "x"
                result,error = calculate(mathematicEquation)

        # fig = pyplot.gcf()
        fig, ax = pyplot.subplots()  # Получаю 2 области, области внутри осей и за осями
        ax.set(facecolor='#191930')
        ax.tick_params(axis='both',which= 'minor',labelcolor = '#00CED1')
        pyplot.plot(result,color='#00CED1')
        pyplot.grid(color='w', linewidth=0.5)

        buf = io.BytesIO()
        fig.savefig(buf,format = "png",facecolor='#38486F')
        buf.seek(0)
        string = base64.b64encode(buf.read())
        uri = urllib.parse.quote(string)
        if 'Save' in request.POST:
                figure = MyModelWithFigure(name = mathematicEquation,blob=buf.getvalue(), author = request.user)
                figure.save()
        pyplot.close(fig)

        return render(request,'Home.html', {'form':form, 'data':uri,'error':error})

def All_plots(request):
        print(request.user)
        figures = MyModelWithFigure.objects.filter(author = request.user)
        images = {}
        for plots in figures:
                string = base64.b64encode(plots.blob)
                uri = urllib.parse.quote(string)
                images[plots.name]=uri
        return render(request,'all_plots.html',{'figures':images})