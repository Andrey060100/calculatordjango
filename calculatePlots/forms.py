from django import forms


class EquationForm(forms.Form):
    use_required_attribute = False
    equation = forms.CharField(label="",max_length = 100)
    xrange1 = forms.IntegerField(label="От:")
    xrange2 = forms.IntegerField(label="До:")
    def __init__(self, *args, **kwargs):
        super(EquationForm, self).__init__(*args, **kwargs)
        self.fields['equation'].required = False
        self.fields['xrange1'].required = False
        self.fields['xrange2'].required = False




